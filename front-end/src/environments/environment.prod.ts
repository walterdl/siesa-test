export const environment = {
  production: true,
  productsEndpoint: 'https://dummy-online-store-backend.walterdl.now.sh/api/products'
};
