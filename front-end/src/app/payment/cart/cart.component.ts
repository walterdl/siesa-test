import { Component, OnInit, Output, EventEmitter } from '@angular/core';

// Own
// Types
import { Product } from '@app/common/types';
// Services
import { CartService } from '@app/payment/cart.service';
import { ProductService } from '@app/common/product.service';

@Component({
  selector: 'app-cart',
  templateUrl: 'cart.component.html'
})
export class CartComponent implements OnInit {
  private cart: Product[];

  public get hasProducts(): boolean {
    return !!this.cart.length;
  }

  public get total(): number {
    return this.cartService.getTotal();
  }

  @Output() public goToCheckout = new EventEmitter<void>();

  constructor(private cartService: CartService, private productService: ProductService) { }

  public ngOnInit(): void {
    this.setInternalCart();
  }

  private setInternalCart() {
    this.cart = this.cartService.cart;
  }

  public getDiscount(product: Product): string {
    if (!product.discount) {
      return '----';
    }

    return `${product.discount.percentage}%`;
  }

  public getProductTotal(product: Product): number {
    return this.productService.getPriceWithDiscount(product);
  }

  public goToCheckoutClick() {
    if (!this.hasProducts) {
      return;
    }

    this.goToCheckout.emit();
  }

  public removeProductFromCart(product: Product) {
    this.cartService.remove(product);
    this.setInternalCart();
  }
}
