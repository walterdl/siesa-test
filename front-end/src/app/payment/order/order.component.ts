import { Component, OnInit } from '@angular/core';

// Own
// Services
import { CartService } from '@app/payment/cart.service';
import { PaymentService } from '@app/payment/payment.service';
import { ProductService } from '@app/common/product.service';

interface ProductForOrder {
  id: string;
  name: string;
  cuantity: number;
  total: number;
}

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html'
})
export class OrderComponent implements OnInit {
  public products: ProductForOrder[];

  public get creditCardNumberHidden(): string {
    return '****' + this.paymentService.creditCard.number.substring(this.paymentService.creditCard.number.length - 4);
  }

  public total: number;

  constructor(private cartService: CartService, private paymentService: PaymentService, private productService: ProductService) { }

  public ngOnInit(): void {
    this.setProductsForOrder();
    this.total = this.cartService.getTotal();
    this.cartService.clearCart();
    this.paymentService.eraseCreditCard();
  }

  private setProductsForOrder() {
    this.products = [];

    this.cartService.cart.forEach(prod => {
      const productForOrder = this.products.find(product => product.id === prod.id);

      if (!!productForOrder) {
        productForOrder.cuantity++;
        productForOrder.total += this.productService.getPriceWithDiscount(prod);
      } else {
        this.products.push({
          id: prod.id,
          name: prod.name,
          cuantity: 1,
          total: this.productService.getPriceWithDiscount(prod)
        });
      }
    });
  }
}
