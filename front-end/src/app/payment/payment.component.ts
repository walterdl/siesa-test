import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { MatHorizontalStepper } from '@angular/material/stepper';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html'
})
export class PaymentComponent implements AfterViewInit {
  @ViewChild('stepper', { static: true }) private stepper: MatHorizontalStepper;
  public cartCompleted = false;
  public checkoutCompleted = false;
  public orderCompleted = false;
  public cartEditable = true;
  public checkoutEditable = true;

  constructor(private toastrService: ToastrService) { }

  public get isInOrder(): boolean {
    return this.stepper.selectedIndex === 2;
  }

  public ngAfterViewInit(): void {
    this.stepper.selectionChange.subscribe((event: StepperSelectionEvent) => {
      if (event.selectedIndex < event.previouslySelectedIndex) {
        this.sendBackPaymentProcess();
      }

      if (event.selectedIndex === 2) {
        this.cartEditable = false;
        this.checkoutEditable = false;
      }
    });
  }

  public evolvePaymentProcess(): void {
    if (!this.cartCompleted) {
      this.cartCompleted = true;
      return this.moveStepper();
    }

    if (!this.checkoutCompleted) {
      this.checkoutCompleted = true;
      this.toastrService.success('Compra realizada');
      return this.moveStepper();
    }
  }

  private moveStepper() {
    // Without this timeout the stepper doesn't really move
    // since the complete state in the same tick is being propagated.
    setTimeout(() => {
      this.stepper.next();
    }, 0);
  }

  public sendBackPaymentProcess(): void {
    if (this.checkoutCompleted) {
      this.checkoutCompleted = false;
      return this.moveBackStepper();
    }

    if (this.cartCompleted) {
      this.cartCompleted = false;
      return this.moveBackStepper();
    }
  }

  private moveBackStepper() {
    setTimeout(() => {
      this.stepper.previous();
    }, 0);
  }
}
