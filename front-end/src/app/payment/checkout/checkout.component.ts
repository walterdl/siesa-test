import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CreditCardValidator } from 'angular-cc-library';

// Own
// Services
import { PaymentService } from '@app/payment/payment.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html'
})
export class CheckoutComponent implements OnInit {
  public form: FormGroup;

  public get invalidCreditCard(): boolean {
    return this.form.get('creditCard').invalid;
  }

  public get invalidExpirationDate(): boolean {
    return this.form.get('expirationDate').invalid;
  }

  public get invalidCvc(): boolean {
    return this.form.get('cvc').invalid;
  }

  @Output() public goToCheckout = new EventEmitter<void>();
  @Output() public goToOrder = new EventEmitter<void>();

  constructor(private formBuilder: FormBuilder, private paymentService: PaymentService) { }

  public ngOnInit(): void {
    this.form = this.formBuilder.group({
      creditCard: [this.paymentService.creditCard.number, [CreditCardValidator.validateCCNumber as any]],
      expirationDate: [this.paymentService.creditCard.expiration, [CreditCardValidator.validateExpDate as any]],
      cvc: [this.paymentService.creditCard.cvc, [Validators.required, Validators.minLength(3) as any, Validators.maxLength(4) as any]]
    });
  }

  public goToCheckoutClick() {
    this.goToCheckout.emit();
  }

  public onSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.paymentService.setCreditCard({
      number: this.form.get('creditCard').value,
      expiration: this.form.get('expirationDate').value,
      cvc: this.form.get('cvc').value
    });

    this.goToOrder.emit();
  }
}
