import { Injectable } from '@angular/core';
import clone from 'clone-deep';

// Own
// Types
import { CreditCard } from '@app/common/types';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  private static defaultCreditCardValue = {
    number: '4565566764344',
    expiration: '02 / 20',
    cvc: '233'
  };
  private static internalCreditCard: CreditCard = PaymentService.defaultCreditCardValue;

  public get creditCard(): CreditCard {
    return clone(PaymentService.internalCreditCard);
  }

  public setCreditCard(creditCard: CreditCard) {
    PaymentService.internalCreditCard = creditCard;
  }

  public eraseCreditCard() {
    PaymentService.internalCreditCard = PaymentService.defaultCreditCardValue;
  }
}
