import { Injectable } from '@angular/core';
import clone from 'clone-deep';

// Own
// Types
import { Product } from '@app/common/types';
// Services
import { ProductService } from '@app/common/product.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private static internalCart: Product[] = [];

  constructor(private productService: ProductService) { }

  public get cart(): Product[] {
    // Return a deep clone of the original cart so client-code couldn't alter it
    return clone(CartService.internalCart);
  }

  public add(product: Product) {
    CartService.internalCart.push(product);
  }

  public remove(product: Product) {
    const indexToRemove = CartService.internalCart.findIndex(prod => prod.id === product.id)

    if (indexToRemove !== -1) {
      CartService.internalCart.splice(indexToRemove, 1);
    }
  }

  public clearCart() {
    CartService.internalCart = [];
  }

  public getTotal(): number {
    let total = 0;

    CartService.internalCart.forEach(product => {
      total += this.productService.getPriceWithDiscount(product);
    });

    return total;
  }
}
