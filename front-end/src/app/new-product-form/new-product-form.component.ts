import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatExpansionPanel } from '@angular/material/expansion';
import { MatSliderChange } from '@angular/material/slider';
import { finalize } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

// Own
// Services
import { StoreService } from '@app/store/store.service';
// Types
import { NewProduct } from '@app/common/types';

@Component({
  selector: 'app-new-product-form',
  templateUrl: './new-product-form.component.html',
  styleUrls: ['./new-product-form.component.scss']
})
export class NewProductFormComponent implements OnInit {
  public form: FormGroup;
  public saving: boolean;
  public error: string;

  @ViewChild('discountPanel', { static: true }) public discountPanel: MatExpansionPanel;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private storeService: StoreService,
    private toastrService: ToastrService) { }

  public get showForm(): boolean {
    return !!(!this.saving && !this.error);
  }

  public get name(): FormControl {
    return this.form.get('name') as FormControl;
  }

  public get description(): FormControl {
    return this.form.get('description') as FormControl;
  }

  public get price(): FormControl {
    return this.form.get('price') as FormControl;
  }

  public get discount(): FormControl {
    return this.form.get('discount') as FormControl;
  }

  public get discountFormated(): string {
    return this.formatPercentageLabel(this.discount.value);
  }

  public get discountStartDateControl(): FormControl {
    return this.form.get('discountStartDate') as FormControl;
  }

  public get discountEndDateControl(): FormControl {
    return this.form.get('discountEndDate') as FormControl;
  }

  public get discountDatesOutOfRange(): boolean {
    if (!this.discountStartDateControl.value || !this.discountEndDateControl.value) {
      return true;
    }

    return (this.discountEndDateControl.value as Date).getTime() < (this.discountStartDateControl.value as Date).getTime();
  }

  public get isFormInvalid(): boolean {
    return !!(this.name.invalid || this.price.invalid || (this.withDiscount && this.discountDatesOutOfRange));
  }

  private withDiscount = false;

  public ngOnInit(): void {
    const _this = this;

    this.form = this.formBuilder.group({
      name: this.formBuilder.control('', [Validators.required]),
      description: this.formBuilder.control(''),
      price: this.formBuilder.control(0, [Validators.required, _this.positiveNumbersValidatorFactory()]),
      discount: this.formBuilder.control(0, [Validators.required, Validators.pattern('^[0-9]*$')]),
      discountStartDate: this.formBuilder.control(null, [Validators.required]),
      discountEndDate: this.formBuilder.control(null, [Validators.required]),
    });
  }


  private positiveNumbersValidatorFactory(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const numberParsed = parseInt(control.value, 10);
      const isPositiveNumber = !isNaN(numberParsed) && numberParsed > 0;
      return isPositiveNumber ? null : { 'no-positive': true };
    };
  }

  public toggleDiscount(event: MatSlideToggleChange) {
    this.withDiscount = event.checked;
    return this.discountPanel.toggle();
  }

  public formatPercentageLabel(value: number | null): string {
    return `${value}%`;
  }

  public changeDiscountPercentage(event: MatSliderChange) {
    this.discount.setValue(event.value);
  }

  public submit(): void {
    if (this.isFormInvalid) {
      return;
    }

    this.prepareForSave();

    this.storeService.addNewProduct(this.getNewProductFromForm())
      .pipe(finalize(() => this.saving = false))
      .subscribe(() => {
        this.toastrService.success('Producto creado');
        this.router.navigate(['/']);
      }, error => {
        this.toastrService.error(error);
      });
  }

  private prepareForSave() {
    this.saving = true;
    this.error = null;
  }

  private getNewProductFromForm(): NewProduct {
    const product: NewProduct = {
      name: this.name.value,
      description: this.description.value,
      price: this.price.value
    };

    if (this.withDiscount) {
      product.discount = {
        percentage: this.discount.value,
        start: this.discountStartDateControl.value,
        end: this.discountEndDateControl.value
      };
    }

    return product;
  }
}
