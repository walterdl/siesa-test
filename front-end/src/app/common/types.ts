export interface NewProduct {
  name: string;
  description?: string;
  price: number;
  discount?: Discount;
}

export interface Product extends NewProduct {
  id: string;
}

export interface Discount {
  percentage: number;
  start: Date;
  end: Date;
}

export interface CreditCard {
  number: string;
  expiration: string;
  cvc: string;
}
