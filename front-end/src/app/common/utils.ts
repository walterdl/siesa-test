export function randomId(): string {
  return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}

/**
 * Checks if the given `date` has the format yyyy:mm:dd hh:mm:ss
 */
export function validateDate(date: string): boolean {
  const dtRegex = new RegExp(/\b\d{4}[\/-]\d{1,2}[\/-]\b\d{1,2} (0\d|1[01]):[0-5]\d:[0-5]\d$\b/);
  return dtRegex.test(date);
}
