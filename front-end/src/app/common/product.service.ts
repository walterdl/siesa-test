import { Injectable } from '@angular/core';

// Own
import { Product } from '@app/common/types';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  public productHasDiscount(product: Product): boolean {
    return !!product.discount;
  }

  public getPriceWithDiscount(product: Product): number {
    if (!this.productHasDiscount(product)) {
      return product.price;
    }

    const discountApplied = product.price - (product.price * (product.discount.percentage / 100));
    return Math.round(discountApplied * 100) / 100; // Round up to 2 decimals
  }

  public getDiscountPeriod(product: Product): string {
    if (!this.productHasDiscount(product)) {
      return '----';
    }

    return `Desde ${product.discount.start.toLocaleDateString()} hasta ${product.discount.end.toLocaleDateString()}`;
  }
}
