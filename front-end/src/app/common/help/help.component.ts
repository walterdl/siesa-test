import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';

// Own
// Declarations
import { HelpDialogComponent } from '@app/common/help/dialog/help-dialog.component'

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html'
})
export class HelpComponent {
  constructor(public dialog: MatDialog) { }

  public showHelp(): void {
    this.dialog.open(HelpDialogComponent, {
      width: '350px'
    });
  }
}
