import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-help-dialog',
  templateUrl: './help-dialog.component.html'
})
export class HelpDialogComponent implements OnInit {
  public help: string;

  constructor(private route: ActivatedRoute) { }

  public ngOnInit(): void {
    this.help = this.route.root.firstChild.snapshot.data.help;
    // console.log(this.route);
    // this.route.data.subscribe(data => {
    //   console.log(data);
    // });

    // console.log(this.route.routeConfig);

    // this.route.root.data.subscribe(data => console.log(data));
    // this.help = this.route.routeConfig.data.help;
  }
}
