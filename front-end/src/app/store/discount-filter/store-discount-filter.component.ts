import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-store-discount-filter',
  templateUrl: './store-discount-filter.component.html'
})
export class StoreDiscountFilterComponent implements OnInit, OnChanges {
  public options = [
    {
      value: 'desc',
      display: 'Recientes'
    },
    {
      value: 'asc',
      display: 'Anteriores'
    }
  ];

  public list: FormControl;

  @Input() public order = 'desc';
  @Output() public filterChange = new EventEmitter<'desc' | 'asc'>();

  public ngOnInit(): void {
    this.list = new FormControl(this.order);
    this.list.valueChanges.subscribe(orderSelected => {
      this.filterChange.emit(orderSelected);
    });
    this.filterChange.emit(this.list.value);
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.order && !changes.order.isFirstChange()) {
      this.list.setValue(changes.order.currentValue);
    }
  }
}
