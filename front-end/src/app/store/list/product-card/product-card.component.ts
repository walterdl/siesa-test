import { Component, Input } from '@angular/core';

// Own
// Types
import { Product } from '@app/common/types';
// Services
import { ProductService } from '@app/common/product.service';
import { CartService } from '@app/payment/cart.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['product-card.component.scss']
})
export class ProductCardComponent {
  @Input() public product: Product;

  constructor(private productService: ProductService, private cartService: CartService) { }

  public get productHasDiscount(): boolean {
    return this.productService.productHasDiscount(this.product);
  }

  public get priceWithDiscount(): number {
    return this.productService.getPriceWithDiscount(this.product);
  }

  public get discountPeriod(): string {
    return this.productService.getDiscountPeriod(this.product)
  }

  public addToCart() {
    this.cartService.add(this.product);
  }
}

