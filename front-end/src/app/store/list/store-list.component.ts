import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';

// Own
// Services
import { StoreService } from '@app/store/store.service';
// Types
import { Product } from '@app/common/types';

@Component({
  selector: 'app-store-list',
  templateUrl: './store-list.component.html',
  styleUrls: ['./store-list.component.scss']
})
export class StoreListComponent implements OnInit {
  public loadingProducts: boolean;
  public products: Product[] = [];
  public error: string;

  public get showProducts(): boolean {
    return !!(!this.loadingProducts && !this.error);
  }

  public get thereAreProducts(): boolean {
    return !!(this.products && this.products.length);
  }

  constructor(private storeListService: StoreService) { }

  public ngOnInit(): void {
    this.load();
  }

  public load() {
    this.prepareForLoad();

    this.storeListService.getItems()
      .pipe(finalize(() => this.loadingProducts = false))
      .subscribe(products => this.products = products,
        error => this.error = error);
  }

  private prepareForLoad() {
    this.error = null;
    this.loadingProducts = true;
  }

  public filterChange(newOrder: 'desc' | 'asc') {
    this.storeListService.orderProducts(newOrder);
  }
}
