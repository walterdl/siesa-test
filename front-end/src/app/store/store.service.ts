import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DateTime } from 'luxon';

// Own
// Types
import { Product, NewProduct } from '@app/common/types';
// Utils
import { validateDate } from '@app/common/utils';
// Constants
import { environment } from '@root/environments/environment';

interface ExternalProduct {
  id: number | string;
  name: string;
  description: string;
  price: number;
  discount?: number;
  discountstart?: string; // Datetime with format yyy-mm-dd hh:mm:ss
  discountend?: string; // Datetime with format yyy-mm-dd hh:mm:ss
}

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  private static products: Product[] = [];

  constructor(private http: HttpClient) { }

  public getItems(): Observable<Product[]> {
    return new Observable<Product[]>(observer => {
      this.http.get(environment.productsEndpoint)
        .subscribe((externalProducts: ExternalProduct[]) => {
          if (!Array.isArray(externalProducts)) {
            return observer.next([]);
          }

          StoreService.products = this.mapExternalProducts(externalProducts);
          observer.next(StoreService.products);
        }, error => observer.error(error),
          () => observer.complete());
    });
  }

  /**
   * Maps `ExternalProduct[]` to `Product[]`.
   */
  private mapExternalProducts(externalProducts: ExternalProduct[]): Product[] {
    const products: Product[] = [];

    externalProducts.forEach(externalProduct => {
      if (externalProduct.discount && (!validateDate(externalProduct.discountstart) || !validateDate(externalProduct.discountend))) {
        return;
      }

      products.push({
        id: externalProduct.id.toString(),
        name: externalProduct.name,
        description: externalProduct.description,
        price: externalProduct.price,
        discount: !externalProduct.discount ? null : {
          percentage: externalProduct.discount,
          start: new Date(externalProduct.discountstart),
          end: new Date(externalProduct.discountend),
        }
      });
    });

    return products;
  }

  public addNewProduct(newProduct: NewProduct): Observable<void> {
    return new Observable<void>(observer => {
      const productToSave = this.mapInternalProducts([newProduct])[0];

      this.http.post(environment.productsEndpoint, productToSave)
        .subscribe(
          (productSaved: ExternalProduct) => {
            StoreService.products.push(this.mapExternalProducts([productSaved])[0]);
            observer.next();
          },
          error => observer.error(error),
          () => observer.complete()
        );
    });
  }

  /**
   * Makes the reverse operation of `mapExternalProducts` from `NewProduct`.
   */
  private mapInternalProducts(products: NewProduct[]): ExternalProduct[] {
    const externalProducts: ExternalProduct[] = [];

    products.forEach(prod => {
      externalProducts.push({
        name: prod.name,
        description: prod.description,
        price: prod.price,
        discount: !prod.discount ? null : prod.discount.percentage,
        discountstart: !prod.discount ? null : this.convertDateToExternalProductDate(prod.discount.start),
        discountend: !prod.discount ? null : this.convertDateToExternalProductDate(prod.discount.end)
      } as any);
    });

    return externalProducts;
  }

  private convertDateToExternalProductDate(date: Date): string {
    return DateTime.fromJSDate(date).toFormat('yyyy-MM-dd HH:mm:ss');
  }

  public orderProducts(order: 'desc' | 'asc') {
    StoreService.products.sort((a: Product, b: Product) => {
      if (!a.discount && !b.discount) {
        return 0;
      }

      if (!a.discount && b.discount) {
        return 1;
      }

      if (!b.discount && a.discount) {
        return -1;
      }

      if (order === 'desc') {
        return a.discount.start.getTime() > b.discount.end.getTime() ? -1 : 1;
      }

      return a.discount.start.getTime() > b.discount.end.getTime() ? 1 : -1;
    });
  }
}
