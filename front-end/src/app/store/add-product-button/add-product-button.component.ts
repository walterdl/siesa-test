import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { MatTooltip } from '@angular/material/tooltip';

@Component({
  selector: 'app-add-product-button',
  template: `
    <div class="add-product-button__container">
      <a routerLink="/new-product">
        <button mat-fab matTooltip="Agregar producto" matTooltipPosition="left" #tooltip="matTooltip">
          <mat-icon>add</mat-icon>
        </button>
      </a>
    </div>
  `,
  styles: [`
    .add-product-button__container {
      position: fixed;
      right: 15px;
      bottom: 30px;
    }
  `]
})
export class AddProductButtonComponent implements AfterViewInit {
  @ViewChild('tooltip', { static: true }) private tooltip: MatTooltip;

  public ngAfterViewInit(): void {
    setTimeout(() => { // Avoid ngAfterViewExpressionChecked error
      this.tooltip.show();
    }, 0);
  }
}
