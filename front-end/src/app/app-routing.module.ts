import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Own
// Declarations
import { StoreListComponent } from '@app/store/list/store-list.component';
import { PaymentComponent } from '@app/payment/payment.component';
import { NewProductFormComponent } from '@app/new-product-form/new-product-form.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: StoreListComponent,
    data: {
      help: `
      Consulta, lista y ordena los productos de la tienda.

      El orden de los productos por descuento se realiza con la fecha de inicio del descuento:
      - Opción "recientes" presenta de primero los productos con fecha de inicio descuento mas reciente.
      - Opción "Anteriores" hace lo contrario.

      Los productos sin descuento se presentan de último.
      `
    }
  },
  {
    path: 'payment',
    component: PaymentComponent,
    data: {
      help: `
      Simula pasarela de pago.

      El formulario del paso "checkout" se prepuebla con una tarjeta de crédito dummy, ya que este formulario
      valida que sea una tarjeta de crédito válida. La tarjeta dummy utilizada es:

      **Tarjeta de crédito**: 4565566764344

      **Fecha de expiración**: 02 / 20

      **cvc**: 233
      `
    }
  },
  {
    path: 'new-product',
    component: NewProductFormComponent,
    data: {
      help: `
      El botón de guardar solo se habilita si el formulario es válido.

      Si el switch que habilita los controles de descuento esta activado y si estos controles son inválidos,
      el botón se inhabilita.

      La fecha de inicio de descuento debe ser menor a la fecha de fin, o de lo contrario se considera formulario inválido.
      `
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
