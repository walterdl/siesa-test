import { Component } from '@angular/core';

// Own
// Services
import { CartService } from '@app/payment/cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  constructor(private cartService: CartService) { }

  public get productsInCart(): number {
    return this.cartService.cart.length;
  }
}
