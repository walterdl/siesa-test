import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CreditCardDirectivesModule } from 'angular-cc-library';
import { ToastrModule } from 'ngx-toastr';
import { MarkdownModule } from 'ngx-markdown';

// Angular material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatBadgeModule } from '@angular/material/badge';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSliderModule } from '@angular/material/slider';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatDialogModule } from '@angular/material/dialog';

// Own
// Router
import { AppRoutingModule } from '@app/app-routing.module';
// Declarations
import { AppComponent } from '@app/app.component';
import { HeaderComponent } from '@app/layout/header/header.component';
import { StoreListComponent } from '@app/store/list/store-list.component';
import { StoreDiscountFilterComponent } from '@app/store/discount-filter/store-discount-filter.component';
import { AddProductButtonComponent } from '@app/store/add-product-button/add-product-button.component';
import { ProductCardComponent } from '@app/store/list/product-card/product-card.component';
import { PaymentComponent } from '@app/payment/payment.component';
import { CartComponent } from '@app/payment/cart/cart.component';
import { CheckoutComponent } from '@app/payment/checkout/checkout.component';
import { OrderComponent } from '@app/payment/order/order.component';
import { NewProductFormComponent } from '@app/new-product-form/new-product-form.component';
import { HelpComponent } from '@app/common/help/help.component';
import { HelpDialogComponent } from '@app/common/help/dialog/help-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    // Layout
    HeaderComponent,
    // Store list
    StoreListComponent,
    StoreDiscountFilterComponent,
    AddProductButtonComponent,
    ProductCardComponent,
    PaymentComponent,
    CartComponent,
    CheckoutComponent,
    OrderComponent,
    NewProductFormComponent,
    HelpComponent,
    HelpDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    CreditCardDirectivesModule,
    ToastrModule.forRoot(),
    MarkdownModule.forRoot(),

    // Angular material
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatBadgeModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatSliderModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    // Router
    AppRoutingModule
  ],
  entryComponents: [
    HelpDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
