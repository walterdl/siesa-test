<?php

namespace app\models;

use yii\db\ActiveRecord;

class Product extends ActiveRecord
{
  public static function tableName()
  {
    return 'product';
  }

  public static function primaryKey()
  {
    return ['id'];
  }

  public function rules()
  {
    return [
      [['name', 'price'], 'required'],
      ['description', 'string'],
      ['discount', 'number'],
      ['discountstart', 'string'],
      ['discountend', 'string']
    ];
  }
}
