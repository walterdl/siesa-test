CREATE DATABASE IF NOT EXISTS `aguitadecoco` DEFAULT CHARACTER SET utf8 ;

USE `aguitadecoco` ;

DROP TABLE IF EXISTS `aguitadecoco`.`product` ;

CREATE TABLE IF NOT EXISTS `aguitadecoco`.`product` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NULL,
  `price` INT NOT NULL,
  `discount` INT NULL,
  `discountstart` DATETIME NULL,
  `discountend` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

INSERT INTO `aguitadecoco`.`product` (`name`, `description`, `price`, `discount`, `discountstart`, `discountend`) VALUES ('Coconudo', 'Coctel especial de la casa', 40, 20, '2008-7-04', '2019-7-04');
INSERT INTO `aguitadecoco`.`product` (`name`, `description`, `price`, `discount`, `discountstart`, `discountend`) VALUES ('Manzada de Coco', 'Fruta manzada cubierta de polvo de Coco', 30, NULL, NULL, NULL);
INSERT INTO `aguitadecoco`.`product` (`name`, `description`, `price`, `discount`, `discountstart`, `discountend`) VALUES ('Tacos Coco', 'Taco comun relleno de Coco', 15, NULL, NULL, NULL);
INSERT INTO `aguitadecoco`.`product` (`name`, `description`, `price`, `discount`, `discountstart`, `discountend`) VALUES ('Coco polvorizado', 'Bolsa pequeña de 150g de coco polvorizado', 50, NULL, NULL, NULL);
INSERT INTO `aguitadecoco`.`product` (`name`, `description`, `price`, `discount`, `discountstart`, `discountend`) VALUES ('Coconuco', 'Coctel #2 especial de la casa', 60, 10, '2019-3-20', '2019-4-15');
INSERT INTO `aguitadecoco`.`product` (`name`, `description`, `price`, `discount`, `discountstart`, `discountend`) VALUES ('Sopa con Tartamesi y Coco', 'Sopa con Tartamesi, albaca y polvorizado de Coco', 120, 20, '2019-1-20', '2019-2-20');
INSERT INTO `aguitadecoco`.`product` (`name`, `description`, `price`, `discount`, `discountstart`, `discountend`) VALUES ('Pasta con alpasto de Coco', 'Pasta italiana con alspasto con Coco', 80, 10, '2019-4-19', '2019-5-01');
