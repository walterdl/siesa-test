<?php

namespace app\controllers;

use yii\rest\ActiveController;

class ProductController extends ActiveController
{
  public $modelClass = 'app\models\Product';

  public $enableCsrfValidation = false;

  public function behaviors()
  {
    $behaviors = parent::behaviors();

    unset($behaviors['authenticator']);

    // add CORS filter
    $behaviors['corsFilter'] = [
      'class' => '\yii\filters\Cors',
      'cors' => [
        'Origin' => ['*'],
        'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
        'Access-Control-Request-Headers' => ['*'],
      ],
    ];

    return $behaviors;
  }
}
