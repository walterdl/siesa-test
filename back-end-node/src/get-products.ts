// Own
import App from './_internal/config/infrastructure';
import getLogger from './_internal/common/util/logger';
import ProductModel, { Product } from './_internal/models/product';
import { prepareProductForResponse } from './_internal/common/util/products';

export default App.createRequestHandler(async (_, res) => {
  const products: Product[] = await ProductModel.find().catch(err => {
    getLogger().error(`There was an error getting products. Details: ${err}`);
    return Promise.reject('Internal server error getting products.');
  });


  if (!Array.isArray(products)) {
    return res.json([]);
  }

  res.json(products.map(prod => prepareProductForResponse(prod)));
});

