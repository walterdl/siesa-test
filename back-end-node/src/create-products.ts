// Own
import App from './_internal/config/infrastructure';
import getLogger from './_internal/common/util/logger';
import ProductModel from './_internal/models/product';
import { prepareProductForResponse } from './_internal/common/util/products';

export default App.createRequestHandler(async (req, res) => {
  const newProduct = new ProductModel();
  newProduct.name = req.body.name;
  newProduct.description = req.body.description;
  newProduct.price = req.body.price;
  newProduct.discount = req.body.discount;
  newProduct.discountstart = req.body.discountstart;
  newProduct.discountend = req.body.discountend;

  await newProduct.save().catch(error => {
    getLogger().error(`There was an error creating product. Details: ${error}`);
    return Promise.reject('Internal server error creating product.');
  });

  res.json(prepareProductForResponse(newProduct));
});

