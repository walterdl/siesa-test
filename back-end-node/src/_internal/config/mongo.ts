import mongoose from 'mongoose';

// Own
import { CONNECTION_STRING } from '../common/constants/mongo';
import getLogger from '../common/util/logger';

let connected: boolean;

/**
 * Performs the connection to Mongo.
 */
export default async function(): Promise<void> {
  if (connected) {
    return;
  }

  return mongoose.connect(CONNECTION_STRING, { useNewUrlParser: true })
    .then(() => {
      getLogger().info(`Mongo DB connected.`);
      connected = true;
    })
    .catch(error => {
      getLogger().error(`Couldn't connect DB. Details: ${error}`);
      return Promise.reject('Internal server error starting repository.');
    });
}
