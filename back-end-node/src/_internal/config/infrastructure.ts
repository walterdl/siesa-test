import { inspect } from 'util'; // or directly
import { NowRequest, NowResponse } from '@now/node';

// Own
// Config
import connectDB from '../config/mongo';
// Utils
import getLogger, { logIncomingRequest } from '../common/util/logger';
// Types
import { sendError } from '../common/util/functions';

type NowHandler = (req: NowRequest, res: NowResponse) => Promise<any>;

export default class App {
  private static req: NowRequest;
  private static res: NowResponse;

  public static createRequestHandler(handler: NowHandler): NowHandler {
    return async (req: NowRequest, res: NowResponse) => {
      App.req = req;
      App.res = res;

      try {
        await App.performSetup();

        if (!res.headersSent) {
          await handler(req, res);
        }
      } catch (error) {
        App.processError(error);
      }
    };
  }

  private static async performSetup() {
    // Show the environment file used
    getLogger().info(
      `Using ${process.env.NODE_ENV} environment to supply config environment variables.`
    );

    this.setupCors();

    if (App.isRequestForOptions()) {
      return App.sendCors();
    }

    await connectDB();
    logIncomingRequest(App.req);
  }

  private static setupCors() {
    App.res.setHeader('Access-Control-Allow-Origin', '*');
    App.res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, POST, GET');
    App.res.setHeader('Access-Control-Allow-Headers', '*');
  }

  private static sendCors() {
    App.res.writeHead(200);
    App.res.end();
  }


  private static isRequestForOptions(): boolean {
    return typeof App.req.method === 'string' && App.req.method.toLowerCase() === 'options';
  }


  private static processError(error: Error) {
      getLogger().error(`Unhandled error catched. Details: ${inspect(error)}`);

      sendError(
        App.res,
        `An error ocurred in the process of the API. Details: ${error.message}`
      );
  }
}
