import { getConfigValue } from '../../common/util/functions';

const mongoConnectionString = getConfigValue('MONGO_CONNECTION_STRING');

if (!mongoConnectionString) {
  throw new Error(
    `Cannot initialize mongo constants due lack of config values.
    None mongo connection string was found in environment values.`
  );
}

export const CONNECTION_STRING = mongoConnectionString;
