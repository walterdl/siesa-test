import { DateTime } from 'luxon';

// Own
import { Product } from '../../models/product';

export function prepareProductForResponse(product: Product): Product {
  return {
    ...product.toObject(),
    id: product.id,
    discountstart: !product.discount ? null : convertDateToExternalProductDate(product.discountstart),
    discountend: !product.discount ? null : convertDateToExternalProductDate(product.discountend)
  };
}

export function convertDateToExternalProductDate(date: Date): string {
  return DateTime.fromJSDate(date).toFormat('yyyy-MM-dd HH:mm:ss');
}
