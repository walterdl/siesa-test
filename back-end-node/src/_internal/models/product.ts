import { Document, Schema, model } from 'mongoose';

const productSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    description: {
      type: String
    },
    price: {
      type: Number,
      required: true
    },
    discount: {
      type: Number
    },
    discountstart: {
      type: Date
    },
    discountend: {
      type: Date
    }
  }
);

export interface Product extends Document {
  id: string;
  name: string;
  description?: string;
  price: number;
  discount: number;
  discountstart: Date;
  discountend: Date;
}

export default model<Product>('Products', productSchema, 'products');
