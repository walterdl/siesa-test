# Prueba de desarrollador Web
> Walter Devia

## Tecnologías

### FrontEnd

Desarrollado con Angular 8 con varias librerías MIT:
- [luxon](https://www.npmjs.com/package/luxon) para dar formato a las fechas provenientes de la API hacia las estructuras de datos internas y viceversa.
- [clone-deep](https://www.npmjs.com/package/clone-deep) para crear y entregar copias desde servicios Angular hacia componentes Angular, evitando mutación del objeto original.
- [ngx-markdown](https://www.npmjs.com/package/ngx-markdown) para presentar en html los mesajes de ayuda que están en sintáxis markdown.
- [ngx-toastr](https://www.npmjs.com/package/ngx-toastr) para presentar mensajes de alerta.

Desplegado en ZEIT Now. [Ir al enlace](https://dummy-online-store-frontend.walterdl.now.sh/).

### BackEnd

#### BackEnd en PHP

Desarrollado con PHP 7 y Yii2 basado de la plantilla estándar. Su base de datos es MySQL.

#### BackEnd en Node

Desarrollado con Node 10 y [TypeScript 3.3](https://www.typescriptlang.org/). Su base de datos es [Mongo](https://www.mongodb.com/). Como el FrontEnd utiliza varias librerías MIT:

- [luxon](https://www.npmjs.com/package/luxon) para dar formato a las fechas que se entregan al consumidor de la API.
- [cors](https://www.npmjs.com/package/cors) para habilitar solicitudes de origen distintos.
- [dotenv](https://www.npmjs.com/package/dotenv) para cargar variables de entorno de archivos `.env`.
- [express](https://www.npmjs.com/package/express) como servidor http.
- [mongoose](https://www.npmjs.com/package/mongoose) para interactuar con la base de datos Mongo.
- [winston](https://www.npmjs.com/package/winston) para registrar mensajes de depuración (logger).

Desplegado en ZEIT Now. Endpoint publicado en la url https://dummy-online-store-backend.walterdl.now.sh/api/products

## Requisitos para ejecutar

- Node 10

### BackEnd en PHP

- PHP 7
- Composer
- MySQL

### BackEnd en Node

- [MongoDB](https://www.mongodb.com/download-center/community)

## Como ejecutar

### BackEnd PHP

- Instalar dependencias con `cd back-end-php && composer install`

- Crear base de datos, objetos y datos iniciales con el script sql que se encuentra en `back-end-php/bd-model.sql`.

  El proyecto Yii esta configurado con usuario y password `root` como credenciales para MySQL.

- Iniciar el servidor web integrado con `php yii serve --port=4201`

### BackEnd Node

- Instalar dependencias con `cd back-end-node && npm install`

- Sembrar base de datos Mongo con `npm run seed`. Esto sembrará datos en un servidor Mongo local. Se puede cambiar la url de la base de datos a sembrar en `back-end-node/seeds/seed.json`.

- Iniciar servidor con `npm run start-dev`. Esto inicia el servidor en `http://localhost:8080`. Si se desea cambiar puerto y cadena de conexión de Node a Mongo realizarlo en el archivo `back-end-node/.env`.

### FrontEnd

- Instalar dependencias con `cd front-end && npm install`

- Ejecutar servidor web integrado con `npm start`. Si se desea cambiar el puerto de escucha hacerlo con `npm start -- --port=<puerto>`

- Acceder a la aplicación por la url `http://localhost:<puerto>`.

**NOTA:** por defecto el front-end solicita datos al back-end con la url `http://localhost:4201`. Si se cambia el puerto de escucha del back-end se debe actualizar esta url en `front-end/src/environments/environments.ts`.